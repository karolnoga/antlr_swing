grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat | scopedStat)+ EOF!
    ;
    
scopedStat
    : LB^ (stat | scopedStat)* RB!
    ;

stat
    : expr SCL -> expr

    | VAR ID ASSIGN expr SCL -> ^(VAR ID) ^(ASSIGN ID expr)
    | VAR ID SCL -> ^(VAR ID)
    | PRINT expr SCL -> ^(PRINT expr)
    | ifStat
    | loopStat

    | SCL ->
    ;
    
loopStat
    : WHILE^ expr scopedStat
    ;
    
ifStat
    : IF^ expr scopedStat (ELSE! scopedStat)?
    ;

expr
    : conditExpr
    | ID (ASSIGN | SUMASS | DIFASS)^ expr
    ;
    
conditExpr
    : logExpT5 (QM^ logExpT5 COL! logExpT5)?
    ;
    
logExpT5
    : logExpT4 (LOR^ logExpT4)*
    ;
    
logExpT4
    : bitwiseExprT2 (LAND^ bitwiseExprT2)*
    ;
    
bitwiseExprT2
    : bitwiseExprT1 (BOR^ bitwiseExprT1)*
    ;
    
bitwiseExprT1
    : logExpT3 (BAND^ logExpT3)*
    ;
    
logExpT3
    : logExpT2 ((EQ | NE)^ logExpT2)?
    ;
    
logExpT2
    : shiftExpr ((GT | GE | LT | LE)^ shiftExpr)?
    ;
    
shiftExpr
    : arithExprT2 ((LSH | RSH)^ arithExprT2)?
    ;

arithExprT2
    : arithExprT1
      ( PLUS^ arithExprT1
      | MINUS^ arithExprT1)*
    ;

arithExprT1
    : logExpT1
      ( MUL^ logExpT1
      | DIV^ logExpT1
      | MOD^ logExpT1)*
    ;
    
logExpT1
    : (NOT^)? atom
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR 
  : 'var'
  ;

PRINT 
  : 'print'
  ;
  
IF 
  : 'if'
  ;

ELSE 
  : 'else'
  ;

WHILE
  : 'while'
  ;

ID 
  : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
  ;

INT 
  : '0'..'9'+
  ;

NL 
  : ('\r'? '\n')+ {$channel = HIDDEN;} 
  ;

WS 
  : (' ' | '\t')+ {$channel = HIDDEN;} 
  ;

LP
	: '('
	;

RP
	:	')'
	;

EQ
  : '=='
  ;

NE
  : '!='
  ;

GE
  : '>='
  ;

LE
  : '<='
  ;

RSH
  : '>>'
  ;

LSH
  : '<<'
  ;

LOR
  : '||'
  ;

LAND
  : '&&'
  ;

SUMASS
  : '+='
  ;

DIFASS
  : '-='
  ;

BOR
  : '|'
  ;

BAND
  : '&'
  ;

MOD
  : '%'
  ;

ASSIGN
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;

LB
  : '{'
  ;

RB
  : '}'
  ;

GT
  : '>'
  ;

LT
  : '<'
  ;

QM
  : '?'
  ;

COL
  : ':'
  ;

NOT
  : '!'
  ;

SCL
  : ';'
  ;
