tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer ifId = 0;
  Integer whileId = 0;
}

prog    
      : (e+=expr | e+=scoped | e+=loop | d+=decl)* {addDeclarations($d);} -> prog(declarations={declarations},statements={$e})
      ;
      
scoped
      : ^(LB {enterScope();} (e+=expr | e+=scoped | e+=loop | d+=decl)* {addDeclarations($d);leaveScope();}) -> scoped(statements={$e})
      ;
      
loop
      : ^(WHILE e1=expr s2=scoped) {++whileId;} -> while(cond={$e1.st},body={$s2.st},id={whileId.toString()})
      ;

decl  
      :   ^(VAR i1=ID) {vars.newSymbol($ID.text);} -> declare(n={$ID.text},s={currentScopeId()})
      ;
      catch [RuntimeException ex] {errorID(ex,$i1);}

expr    
      : ^(PLUS   e1=expr e2=expr) -> add     (p1={$e1.st},p2={$e2.st})
      | ^(MINUS  e1=expr e2=expr) -> subtract(p1={$e1.st},p2={$e2.st})
      | ^(MUL    e1=expr e2=expr) -> multiply(p1={$e1.st},p2={$e2.st})
      | ^(DIV    e1=expr e2=expr) -> divide  (p1={$e1.st},p2={$e2.st})
      | ^(ASSIGN i1=ID   e2=expr) -> assign  (n={$i1},s={getSymbolScopeId($i1.text)},p1={$e2.st})
      | INT                       -> int     (i={$INT.text})
      | i1=ID                     -> id      (n={$i1.text},s={getSymbolScopeId($i1.text)})
      | ^(IF e1=expr s2=scoped s3=scoped?) {++ifId;} -> if(cond={$e1.st},e1={$s2.st},e2={$s3.st},id={ifId.toString()})
      ;
      catch [RuntimeException ex] {errorID(ex,$i1);}
