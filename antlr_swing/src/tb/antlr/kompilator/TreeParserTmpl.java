/**
 * 
 */
package tb.antlr.kompilator;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;

import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.TreeNodeStream;
import org.antlr.runtime.tree.TreeParser;
import org.antlr.stringtemplate.StringTemplate;

import tb.antlr.symbolTable.LocalSymbols;

/**
 * @author tb
 *
 */
public class TreeParserTmpl extends TreeParser {

	protected LocalSymbols vars = new LocalSymbols();
	protected ArrayList<StringTemplate> declarations = new ArrayList<StringTemplate>();
	protected ArrayDeque<Integer> scopes = new ArrayDeque<Integer>();
	protected int scopeId = 0;
	
	/**
	 * @param input
	 */
	public TreeParserTmpl(TreeNodeStream input) {
		super(input);
		scopes.push(scopeId++);  // dodajemy blok globalny
	}

	/**
	 * @param input
	 * @param state
	 */
	public TreeParserTmpl(TreeNodeStream input, RecognizerSharedState state) {
		super(input, state);
		scopes.push(scopeId++);  // dodajemy blok globalny
	}

	protected void errorID(RuntimeException ex, CommonTree id) {
		System.err.println(ex.getMessage() + ": line " + id.getLine());
	}

	protected void printError(RuntimeException ex) {
		System.err.println(ex.getMessage());
	}
	
	protected void enterScope() {
		vars.enterScope();
		scopes.push(scopeId++);
	}
	
	protected void leaveScope() {
		vars.leaveScope();
		scopes.pop();
	}
	
	protected void addDeclarations(Collection<StringTemplate> dec) {
		if (dec != null)
			declarations.addAll(dec);
	}
	
	/**
	 * Zwraca unikatowe ID aktualnego bloku (0 dla bloku globalnego)
	 */
	protected Integer currentScopeId() {
		return scopes.peek();
	}

	/**
	 * Zwraca unikatowe ID bloku w ktorym zdefiniowano podana zmienna,
	 * rzuca wyjatek RuntimeException, gdy zmiennej nie zdefiniowano
	 */
	protected String getSymbolScopeId(String symbol) {
		Integer depth = vars.symbolDepth(symbol);
		if (depth == null)
			throw new RuntimeException(String.format("Symbol %s not found in scope", symbol));
		Integer scope = scopes.toArray(new Integer[0])[depth];
		return scope.toString();
	}
}
