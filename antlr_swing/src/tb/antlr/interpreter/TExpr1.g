tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    
    : (scoped|expr|stat)* 
    ;
    
scoped
    : ^(LB {vars.enterScope();} (expr|stat)+) {vars.leaveScope();}
    ;

expr returns [Integer out]
    : ^(PLUS   e1=expr e2=expr) {$out = $e1.out + $e2.out;}
    | ^(MINUS  e1=expr e2=expr) {$out = $e1.out - $e2.out;}
    | ^(MUL    e1=expr e2=expr) {$out = $e1.out * $e2.out;}
    | ^(DIV    e1=expr e2=expr) {$out = $e1.out / $e2.out;}
    | ^(MOD    e1=expr e2=expr) {$out = $e1.out \% $e2.out;}
    | ^(LOR    e1=expr e2=expr) {$out = toBool($e1.out) | toBool($e2.out);}
    | ^(LAND   e1=expr e2=expr) {$out = toBool($e1.out) & toBool($e2.out);}
    | ^(BOR    e1=expr e2=expr) {$out = $e1.out | $e2.out;}
    | ^(BAND   e1=expr e2=expr) {$out = $e1.out & $e2.out;}
    | ^(EQ     e1=expr e2=expr) {$out = $e1.out == $e2.out ? 1 : 0;}
    | ^(NE     e1=expr e2=expr) {$out = $e1.out != $e2.out ? 1 : 0;}
    | ^(GT     e1=expr e2=expr) {$out = $e1.out > $e2.out ? 1 : 0;}
    | ^(GE     e1=expr e2=expr) {$out = $e1.out >= $e2.out ? 1 : 0;}
    | ^(LT     e1=expr e2=expr) {$out = $e1.out < $e2.out ? 1 : 0;}
    | ^(LE     e1=expr e2=expr) {$out = $e1.out <= $e2.out ? 1 : 0;}
    | ^(LSH    e1=expr e2=expr) {$out = $e1.out << $e2.out;}
    | ^(RSH    e1=expr e2=expr) {$out = $e1.out >> $e2.out;}
    | ^(NOT    e1=expr)         {$out = $e1.out != 0 ? 0 : 1;}
    | ^(ASSIGN i1=ID   e2=expr) {$out = vars.setSymbol($i1.text, $e2.out);}
    | ^(SUMASS i1=ID   e2=expr) {$out = vars.setSymbol($i1.text, vars.getSymbol($i1.text) + $e2.out);}
    | ^(DIFASS i1=ID   e2=expr) {$out = vars.setSymbol($i1.text, vars.getSymbol($i1.text) - $e2.out);}
    | INT                       {$out = getInt($INT.text);}
    | ID                        {$out = vars.getSymbol($ID.text);}  
    | ^(QM con=expr et=expr ef=expr) {$out = $con.out != 0 ? $et.out : $ef.out;}
    ;
catch [RuntimeException e] { printError(e); }
    
    
stat
    : ^(VAR    i1=ID)           {vars.newSymbol($i1.text);}
    | ^(PRINT e1=expr)          {printValue($e1.out);}
    ;
    
catch [RuntimeException e] { printError(e); }
