package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.LocalSymbols;

public class MyTreeParser extends TreeParser {	
	
	protected LocalSymbols vars = new LocalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void print(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

    protected void printValue(Integer val) {
    	if (val != null)
    		System.out.println(val);
    }

    protected void printError(RuntimeException ex) {
        System.out.println("ERROR: " + ex.getMessage().replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}

	protected Integer toBool(Integer val) {
		return val != 0 ? 1 : 0;
	}
}
